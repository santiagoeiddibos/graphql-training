const { ApolloServer } = require('apollo-server');
const fs = require('fs');
const path = require('path');
/*
	The links variable is used to store the links at runtime.
	For now, everything is stored only in-memory rather than being persisted in a database.
*/
let links = [{
	id: 'link-0',
	url: 'www.howtographql.com',
	description: 'Fullstack tutorial for GraphQL.'
}];
/*
	You’re adding a new integer variable that simply serves as
	a very rudimentary way to generate unique IDs for newly created Link elements.
*/
let idCount = links.length;

/*
	The resolvers object is the actual implementation of the GraphQL schema.
	Notice how its structure is identical to the structure
	of the type definition inside typeDefs: Query.info. 
*/
const resolvers = {
	Query: {
		info: () => `This is the API for Hackernews Clone.`,
		/*
			You’re adding a new resolver for the feed root field.
			Notice that a resolver always has to be named exactly after the corresponding field from the schema definition.	 
		*/	
		feed: () => links,
		link: (parent, args) => {
			const link = links.filter(link => link.id === args.id)[0];
			return link;
		}
	},
	Mutation: {
		/*
			The implementation of the post resolver first creates a new link object,
			then adds it to the existing links list and finally returns the new link. 
		*/
		postLink: (parent, args) => {
			const link = {
				id: `link-${idCount++}`,
				description: args.description,
				url: args.url
			}
			links.push(link);
			return link;
		},

		updateLink: (parent, args) => {
			const findLinkIndex = links.findIndex(link => link.id === args.id);
			const updateLink = links[findLinkIndex] = {
				id: args.id,
				description: args.description,
				url: args.url
			}
			return updateLink;
		},

		deleteLink: (parent, args) => {
			const link = links.filter(link => link.id === args.id)[0];
			const findLinkIndex = links.findIndex(link => link.id === args.id);
			const deleteIndex = links.splice(findLinkIndex, 1);
			return link;
		}
	},
	/*
		Finally, you’re adding three more resolvers for the fields on the Link type from the schema definition.
		We’ll discuss what the parent argument that’s passed into the resolver here is in a bit.
	*/
	Link: {
		id: (parent) => parent.id,
		description: (parent) => parent.description,
		url: (parent) => parent.url
	}
};

/*
	Finally, the schema and resolvers are bundled and passed to ApolloServer which is imported from apollo-server.
	This tells the server what API operations are accepted and how they should be resolved.
*/
const server = new ApolloServer({
	typeDefs: fs.readFileSync(
		path.join(__dirname, 'schema.graphql'),
		'utf-8'
	),
	resolvers
});


server
	.listen()
	.then(
		({ url }) => console.log(`server is running on ${url}.`)
	);